<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\questionnaire;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //gets the auth to see if user is logged in.
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //gets the author id who is creating the questionnaire.
      $questionnaires = Questionnaire::where('author_id', Auth::user()->id)->get();
      //returns the home page view along with the questionnaires that have been created.
        return view('home')->with('questionnaires', $questionnaires);
    }
}
