<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\questionnaire;
use App\Category;


class QuestionnaireController extends Controller



{
  /*
  * Secure the set of pages to the admin.
  */
 public function __construct()
 {
     //gets the auth to see if user is logged in.
     $this->middleware('auth');
 }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/questions2');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     //Function to get to the create form.
    public function create()
    {
        // $cats = Category::lists('title', 'id');

        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //requests the author id and allows them to create a questionnaire.
        $input = $request->all();
        $input['author_id'] = Auth::user()->id;
        $questionnaire = questionnaire::create($input);

        //returns the completed to the questionnaire page along with the questionnaire id.
        return redirect('questionnaire/' . $questionnaire->id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //returns the views of the questionnaires on the questionnaire page.
      $questionnaire = questionnaire::findOrFail($id);
        return view('questionnaire')->with('questionnaire', $questionnaire);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
