<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\question;
use Auth;
use App\Http\Requests;

class QuestionsController extends Controller
{
  public function __construct()
  {
    //gets the auth to see if user is logged in.
      $this->middleware('auth');
  }
     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index()
     {

         return view('questions');

     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */

      //Function to get to the create form.
     public function create()
     {
         // $cats = Category::lists('title', 'id');

         return view('questionnaire');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
       //requests the author id and allows them to create a question.
         $input = $request->all();
         $input['author_id'] = Auth::user()->id;
         $question = question::create($input);

           //returns the completed to the questions page along with the question id.
         return redirect('questions/' . $question->id);


     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
       //returns the views of the questions on the question page.
       $question = question::findOrFail($id);
         return view('question')->with('question', $question);
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
     }
  }
