<?php

//=============================
//STATIC PAGES ================
//=============================

//Show a static view for your home page (app/resources/views/home.blade.php)
// slash means home or index.
//Landing page

//
// Route::get('/', function () {
//     return view('welcome');
// });

Route::resource('/', 'HomeController');

//Questions page
Route::resource('/questionnaire', 'QuestionnaireController');

//Answers page
Route::resource('answers', 'AnswersController');

Route::group(['middleware' => ['web']], function () {

    Route::auth();

Route::resource('/questionnaire', 'QuestionnaireController');

    Route::auth();

  Route::resource('/questions', 'QuestionsController');

        Route::auth();

Route::get('/home', 'HomeController@index');

});

    Route::auth();

Route::get('/home', 'HomeController@index');
