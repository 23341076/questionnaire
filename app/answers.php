<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answers extends Model
{
  protected $fillable = [
    'id number',
    'title',
    'content',
  ];
}
