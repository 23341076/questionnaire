<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
      'title',
      'questions_id',
      'questionnaire_id',
      'author_id',
    ];
}
