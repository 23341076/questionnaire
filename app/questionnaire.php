<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
      'title', 
      'content',
      'slug',
      'author_id',
    ];
}
