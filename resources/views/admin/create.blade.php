@extends('layouts.app')


@section('content')
<div class='container'>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Questionnaire</title>
</head>
<body>
<h1>Add Questionnaire</h1>

{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestion')) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Detail:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}

</body>
</html>

</div>
@endsection
