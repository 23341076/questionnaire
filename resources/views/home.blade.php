@extends('layouts.app')

@section('content')
<div class='container'>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  <!doctype html>
                  <html lang="en">
                  <head>
                      <meta charset="UTF-8">
                      <title>Questionnaires</title>
                  </head>
                  <body>
                  <h1>Questionnaires</h1>



                  <section>
                      @if (isset ($questionnaires))

                          <ul>
                              @foreach ($questionnaires as $questionnaire)
                                  <li><a href="/questionnaire/{{ $questionnaire->id }}"->{{ $questionnaire->title }}</a></li>
                              @endforeach
                          </ul>
                      @else
                          <p> No questions added yet! </p>
                      @endif
                  </section>

                  <a href="/questionnaire/create" > Add Questionnaire </a>

                  </body>
                  </html>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
