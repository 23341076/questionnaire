@extends('layouts.app')

@section('content')

  <div class='container'>

{{ $questionnaire->title }}

{!! Form::open(array('action' => 'QuestionsController@store', 'id' => 'createquestion')) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Question:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Answer 1:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Answer 2:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Answer 3:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
  </div>
{!! Form::close() !!}

@endsection
