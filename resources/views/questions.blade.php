@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  <!doctype html>
                  <html lang="en">
                  <head>
                      <meta charset="UTF-8">
                      <title>Questions</title>
                  </head>
                  <body>
                  <h1>Questions</h1>



                  <section>
                      @if (isset ($questions))

                          <ul>
                              @foreach ($questions as $question)
                                  <li><a href="/questions/{{ $question->id }}"->{{ $question->title }}</a></li>
                              @endforeach
                          </ul>
                      @else
                          <p> No questions added yet! </p>
                      @endif
                  </section>

                  <a href="questionnaire" > Add Questions </a>

                  </body>
                  </html>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
